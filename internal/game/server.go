package game

import (
	"log"

	"github.com/juju/errors"
	multiplayer "gitlab.com/go-games1/multiplayer-networking"
)

type Server struct {
	logger            *log.Logger
	multiplayerServer *multiplayer.MultiplayerServer
}

func NewServer(logger *log.Logger, server *multiplayer.MultiplayerServer) *Server {
	return &Server{
		logger:            logger,
		multiplayerServer: server,
	}
}

func (s *Server) Start() error {
	if err := s.multiplayerServer.Listen(); err != nil {
		return errors.Trace(err)
	}

	return nil
}
