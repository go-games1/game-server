module lab/game-server

go 1.20

require (
	github.com/juju/errors v1.0.0
	gitlab.com/go-games1/multiplayer-networking v0.0.0-00010101000000-000000000000
)

replace gitlab.com/go-games1/multiplayer-networking => ../multiplayer-network
