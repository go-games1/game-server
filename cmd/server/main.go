package main

import (
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"time"

	"lab/game-server/internal/game"

	"github.com/juju/errors"
	multiplayer "gitlab.com/go-games1/multiplayer-networking"
	"gitlab.com/go-games1/multiplayer-networking/transport/packet"
	"gitlab.com/go-games1/multiplayer-networking/transport/session"
)

func main() {
	gob.Register(session.Action(""))

	logger := log.New(os.Stdout, "", 0)

	multiplayerServer, err := multiplayer.NewMultiplayerServer(
		logger,
		4000,
		3,
		time.Second*3,
		func(p packet.Packet) error {
			return errors.Trace(nil)
		},
	)
	if err != nil {
		panic(err)
	}

	fmt.Println("Listening on address", "127.0.0.1:4000")

	gameServer := game.NewServer(logger, multiplayerServer)

	if err := gameServer.Start(); err != nil {
		log.Fatal(err)
	}
}
